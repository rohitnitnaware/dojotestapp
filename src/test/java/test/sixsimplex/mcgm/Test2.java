package test.sixsimplex.mcgm;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;

public class Test2 {

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	@Test
	public void testapp() {
		Client client = Client.create();
		WebResource webResource = client.resource("http://localhost:8080/MavanProject/services/service1");
		ClientResponse response= webResource.accept("text/plain").get(ClientResponse.class);
		Assert.assertEquals(200, response.getStatus());
	}

	@Test
	public void testapp1() {
		Client client = Client.create();
		WebResource webResource = client.resource("http://localhost:8080/MavanProject/services/service4");
		ClientResponse response1 = webResource.accept("text/plain").get(ClientResponse.class);
		Assert.assertEquals(404, response1.getStatus());
	}

}
