package com.sixsimplex.mcgm;

import java.util.HashSet;
import java.util.Set;

import javax.servlet.ServletContext;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.Context;

import test.sixsimplex.mcgm.Test2;

public class TestServicesWebApp extends Application
{
	@Context ServletContext servletContext;
	
	@Override
	public Set<Class<?>> getClasses() 
	{
		Set<Class<?>> classesSet = new HashSet<Class<?>>();
		classesSet.add(ServicesResource.class);
		classesSet.add(Test2.class);
		
		return classesSet;
	}
}
