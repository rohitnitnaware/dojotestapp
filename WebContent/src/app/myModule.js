define([
    'dojo/dom',"dijit/Dialog","dijit/registry","dijit/layout/BorderContainer",
    "dijit/layout/TabContainer","dijit/layout/ContentPane","dojo/domReady!"
],function(dom,Dialog,registry,BorderContainer,TabContainer,ContentPane){
    var oldText = {};
    var dialog = new Dialog({
        title: "Welcome back " + config.app.userName,
        content: "<pre>" + JSON.stringify(config, null, "\t")
    });

    // Draw on the app config to put up a personalized message
    dialog.show();
    return{
        setText: function(id,text){
            var node = dom.byId(id);
            oldText[id] = node.innerHTML;
            node.innerHTML = text;
        },
        restoreText:function(id){
            var node = dom.byId(id);
            node.innerHTML = oldText[id];
            delete(oldText[id]);
        }
    };
});