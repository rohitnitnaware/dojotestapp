define(function(){
	var privateValue = 0;
	return{
		increament : function(){
			privateValue++;
		},
		decrement: function(){
			privateValue--;
		},
		getValue: function(){
			return privateValue;
		}
	};
});